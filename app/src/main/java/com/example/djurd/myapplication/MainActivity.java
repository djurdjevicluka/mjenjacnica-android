package com.example.djurd.myapplication;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("INFO","Create");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        final EditText iznos=findViewById(R.id.iznos);
        final TextView provizija=findViewById(R.id.provizija);
        final TextView krazlika=findViewById(R.id.krazlika);
        final TextView isplata=findViewById(R.id.isplata);
        final TextView textView11=findViewById(R.id.textView);
        final ImageView audSlika=findViewById(R.id.audSlika);
        final ImageView cadSlika=findViewById(R.id.cadSlika);
        final ImageView hrkSlika=findViewById(R.id.hrkSlika);
        final ImageView dkkSlika=findViewById(R.id.dkkSlika);
        final ImageView jpySlika=findViewById(R.id.jpySlika);
        final ImageView nokSlika=findViewById(R.id.nokSlika);
        final ImageView sekSlika=findViewById(R.id.sekSlika);
        final ImageView chfSlika=findViewById(R.id.chfSlika);
        final ImageView gbpSlika=findViewById(R.id.gbpSlika);
        final ImageView usdSlika=findViewById(R.id.usaSlika);
        final ImageView rsdSlika=findViewById(R.id.rsdSlika);
        final ImageView trySlika=findViewById(R.id.trySlika);
        final ImageView eurSlika=findViewById(R.id.eurSlika);
        final ImageView zemSlika=findViewById(R.id.zemSlika);
        Button vaud=findViewById(R.id.vaud);
        Button vcad=findViewById(R.id.vcad);
        Button vhrk=findViewById(R.id.vhrk);
        Button vdkk=findViewById(R.id.vdkk);
        Button vjpy=findViewById(R.id.vjpy);
        Button vnok=findViewById(R.id.vnok);
        Button vsek=findViewById(R.id.vsek);
        Button vchf=findViewById(R.id.vchf);
        Button vgbp=findViewById(R.id.vgbp);
        Button vusd=findViewById(R.id.vusd);
        Button vrsd=findViewById(R.id.vrsd);
        Button vtry=findViewById(R.id.vtry);
        Button veur=findViewById(R.id.veur);
        Button reset=findViewById(R.id.reset);
        zemSlika.setVisibility(View.VISIBLE);
        audSlika.setVisibility(View.INVISIBLE);
        cadSlika.setVisibility(View.INVISIBLE);
        hrkSlika.setVisibility(View.INVISIBLE);
        dkkSlika.setVisibility(View.INVISIBLE);
        nokSlika.setVisibility(View.INVISIBLE);
        sekSlika.setVisibility(View.INVISIBLE);
        trySlika.setVisibility(View.INVISIBLE);
        jpySlika.setVisibility(View.INVISIBLE);
        usdSlika.setVisibility(View.INVISIBLE);
        eurSlika.setVisibility(View.INVISIBLE);
        gbpSlika.setVisibility(View.INVISIBLE);
        chfSlika.setVisibility(View.INVISIBLE);
        rsdSlika.setVisibility(View.INVISIBLE);

        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isplata.setText("");
                krazlika.setText("");
                provizija.setText("");
                iznos.setText("");
                textView11.setText("UNESITE VALUTNI IZNOS");
                zemSlika.setVisibility(View.VISIBLE);
                audSlika.setVisibility(View.INVISIBLE);
                cadSlika.setVisibility(View.INVISIBLE);
                hrkSlika.setVisibility(View.INVISIBLE);
                dkkSlika.setVisibility(View.INVISIBLE);
                nokSlika.setVisibility(View.INVISIBLE);
                sekSlika.setVisibility(View.INVISIBLE);
                trySlika.setVisibility(View.INVISIBLE);
                jpySlika.setVisibility(View.INVISIBLE);
                usdSlika.setVisibility(View.INVISIBLE);
                eurSlika.setVisibility(View.INVISIBLE);
                gbpSlika.setVisibility(View.INVISIBLE);
                chfSlika.setVisibility(View.INVISIBLE);
                rsdSlika.setVisibility(View.INVISIBLE);
            }
        });
        vaud.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //aud
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.VISIBLE);
                    cadSlika.setVisibility(View.INVISIBLE);
                    hrkSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.INVISIBLE);
                    nokSlika.setVisibility(View.INVISIBLE);
                    sekSlika.setVisibility(View.INVISIBLE);
                    trySlika.setVisibility(View.INVISIBLE);
                    jpySlika.setVisibility(View.INVISIBLE);
                    usdSlika.setVisibility(View.INVISIBLE);
                    eurSlika.setVisibility(View.INVISIBLE);
                    gbpSlika.setVisibility(View.INVISIBLE);
                    chfSlika.setVisibility(View.INVISIBLE);
                    rsdSlika.setVisibility(View.INVISIBLE);
                    textView11.setText("Izabrana valuta je AUD");
                    String BAM,proviz="0.00";
                    int provjera;
                    double knovca,kurs=1.20295;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else{
                        double kursnarazlika=0.04114*knovca;
                        double bam=(knovca*kurs);
                        bam=Math.round(bam*100.0)/100.0;
                        kursnarazlika=Math.round(kursnarazlika*100)/100.0;
                        BAM=String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM)+" BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }
            }
        });
        vcad.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //cad
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.INVISIBLE);
                    cadSlika.setVisibility(View.VISIBLE);
                    hrkSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.INVISIBLE);
                    nokSlika.setVisibility(View.INVISIBLE);
                    sekSlika.setVisibility(View.INVISIBLE);
                    trySlika.setVisibility(View.INVISIBLE);
                    jpySlika.setVisibility(View.INVISIBLE);
                    usdSlika.setVisibility(View.INVISIBLE);
                    eurSlika.setVisibility(View.INVISIBLE);
                    gbpSlika.setVisibility(View.INVISIBLE);
                    chfSlika.setVisibility(View.INVISIBLE);
                    rsdSlika.setVisibility(View.INVISIBLE);
                    textView11.setText("Izabrana valuta je CAD");
                    String BAM,proviz="0.00";
                    double knovca,kurs=1.22840;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    int provjera;
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else {
                        double kursnarazlika = 0.03546 * knovca;
                        double bam = (knovca * kurs);
                        bam = Math.round(bam * 100.0) / 100.0;
                        kursnarazlika = Math.round(kursnarazlika * 100.0) / 100.0;
                        BAM = String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM) + " BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }

            }
        });
        vhrk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //hrk
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.INVISIBLE);
                    cadSlika.setVisibility(View.INVISIBLE);
                    hrkSlika.setVisibility(View.VISIBLE);
                    dkkSlika.setVisibility(View.INVISIBLE);
                    nokSlika.setVisibility(View.INVISIBLE);
                    sekSlika.setVisibility(View.INVISIBLE);
                    trySlika.setVisibility(View.INVISIBLE);
                    jpySlika.setVisibility(View.INVISIBLE);
                    usdSlika.setVisibility(View.INVISIBLE);
                    eurSlika.setVisibility(View.INVISIBLE);
                    gbpSlika.setVisibility(View.INVISIBLE);
                    chfSlika.setVisibility(View.INVISIBLE);
                    rsdSlika.setVisibility(View.INVISIBLE);
                    textView11.setText("Izabrana valuta je HRK");
                    String BAM,proviz="0.00";
                    double knovca,kurs=25.67002;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    knovca=knovca/100;
                    int provjera;
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else {
                        double kursnarazlika = 0.60566 * knovca;
                        double bam = knovca * kurs;
                        bam = Math.round(bam * 100.0) / 100.0;
                        kursnarazlika = Math.round(kursnarazlika * 100.0) / 100.0;
                        BAM = String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM) + " BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }

            }
        });
        vdkk.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //dkk
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.INVISIBLE);
                    cadSlika.setVisibility(View.INVISIBLE);
                    hrkSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.VISIBLE);
                    nokSlika.setVisibility(View.INVISIBLE);
                    sekSlika.setVisibility(View.INVISIBLE);
                    trySlika.setVisibility(View.INVISIBLE);
                    jpySlika.setVisibility(View.INVISIBLE);
                    usdSlika.setVisibility(View.INVISIBLE);
                    eurSlika.setVisibility(View.INVISIBLE);
                    gbpSlika.setVisibility(View.INVISIBLE);
                    chfSlika.setVisibility(View.INVISIBLE);
                    rsdSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.VISIBLE);
                    textView11.setText("Izabrana valuta je DKK");
                    String BAM,proviz="0.00";
                    double knovca,kurs=0.25748;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    int provjera;
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else {
                        double kursnarazlika = 0.00527 * knovca;
                        double bam = (knovca * kurs);
                        bam = Math.round(bam * 100.0) / 100.0;
                        kursnarazlika = Math.round(kursnarazlika * 100.0) / 100.0;
                        BAM = String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM) + " BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }
            }
        });
        vjpy.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //jpy
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.INVISIBLE);
                    cadSlika.setVisibility(View.INVISIBLE);
                    hrkSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.INVISIBLE);
                    nokSlika.setVisibility(View.INVISIBLE);
                    sekSlika.setVisibility(View.INVISIBLE);
                    trySlika.setVisibility(View.INVISIBLE);
                    jpySlika.setVisibility(View.VISIBLE);
                    usdSlika.setVisibility(View.INVISIBLE);
                    eurSlika.setVisibility(View.INVISIBLE);
                    gbpSlika.setVisibility(View.INVISIBLE);
                    chfSlika.setVisibility(View.INVISIBLE);
                    rsdSlika.setVisibility(View.INVISIBLE);
                    textView11.setText("Izabrana valuta je JPY");
                    String BAM,proviz="0.00";
                    double knovca,kurs=1.43471;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    knovca=knovca/100;
                    int provjera;
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else {
                        double kursnarazlika = 0.02934 * knovca;
                        double bam = knovca * kurs;
                        bam = Math.round(bam * 100.0) / 100.0;
                        kursnarazlika = Math.round(kursnarazlika * 100.0) / 100.0;
                        BAM = String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM) + " BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }
            }
        });
        vnok.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //nok
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.INVISIBLE);
                    cadSlika.setVisibility(View.INVISIBLE);
                    hrkSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.INVISIBLE);
                    nokSlika.setVisibility(View.VISIBLE);
                    sekSlika.setVisibility(View.INVISIBLE);
                    trySlika.setVisibility(View.INVISIBLE);
                    jpySlika.setVisibility(View.INVISIBLE);
                    usdSlika.setVisibility(View.INVISIBLE);
                    eurSlika.setVisibility(View.INVISIBLE);
                    gbpSlika.setVisibility(View.INVISIBLE);
                    chfSlika.setVisibility(View.INVISIBLE);
                    rsdSlika.setVisibility(View.INVISIBLE);
                    textView11.setText("Izabrana valuta je NOK");
                    String BAM,proviz="0.00";
                    double knovca,kurs=0.19301;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    int provjera;
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else {
                        double kursnarazlika = 0.0066 * knovca;
                        double bam = (knovca * kurs);
                        bam = Math.round(bam * 100.0) / 100.0;
                        kursnarazlika = Math.round(kursnarazlika * 100.0) / 100.0;
                        BAM = String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM) + " BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }
            }
        });
        vsek.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //sek
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.INVISIBLE);
                    cadSlika.setVisibility(View.INVISIBLE);
                    hrkSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.INVISIBLE);
                    nokSlika.setVisibility(View.INVISIBLE);
                    sekSlika.setVisibility(View.VISIBLE);
                    trySlika.setVisibility(View.INVISIBLE);
                    jpySlika.setVisibility(View.INVISIBLE);
                    usdSlika.setVisibility(View.INVISIBLE);
                    eurSlika.setVisibility(View.INVISIBLE);
                    gbpSlika.setVisibility(View.INVISIBLE);
                    chfSlika.setVisibility(View.INVISIBLE);
                    rsdSlika.setVisibility(View.INVISIBLE);
                    textView11.setText("Izabrana valuta je SEK");
                    String BAM,proviz="0.00";
                    double knovca,kurs=0.19017;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    int provjera;
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else {
                        double kursnarazlika = 0.0065 * knovca;
                        double bam = (knovca * kurs);
                        bam = Math.round(bam * 100.0) / 100.0;
                        kursnarazlika = Math.round(kursnarazlika * 100.0) / 100.0;
                        BAM = String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM) + " BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }
            }
        });
        vchf.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //chf
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.INVISIBLE);
                    cadSlika.setVisibility(View.INVISIBLE);
                    hrkSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.INVISIBLE);
                    nokSlika.setVisibility(View.INVISIBLE);
                    sekSlika.setVisibility(View.INVISIBLE);
                    trySlika.setVisibility(View.INVISIBLE);
                    jpySlika.setVisibility(View.INVISIBLE);
                    usdSlika.setVisibility(View.INVISIBLE);
                    eurSlika.setVisibility(View.INVISIBLE);
                    gbpSlika.setVisibility(View.INVISIBLE);
                    chfSlika.setVisibility(View.VISIBLE);
                    rsdSlika.setVisibility(View.INVISIBLE);
                    textView11.setText("Izabrana valuta je CHF");
                    String BAM,proviz="0.00";
                    double knovca,kurs=1.65811;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    int provjera;
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else {
                        double kursnarazlika = 0.04261 * knovca;
                        double bam = (knovca * kurs);
                        bam = Math.round(bam * 100.0) / 100.0;
                        kursnarazlika = Math.round(kursnarazlika * 100.0) / 100.0;
                        BAM = String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM) + " BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }
            }
        });
        vgbp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //gbp
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.INVISIBLE);
                    cadSlika.setVisibility(View.INVISIBLE);
                    hrkSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.INVISIBLE);
                    nokSlika.setVisibility(View.INVISIBLE);
                    sekSlika.setVisibility(View.INVISIBLE);
                    trySlika.setVisibility(View.INVISIBLE);
                    jpySlika.setVisibility(View.INVISIBLE);
                    usdSlika.setVisibility(View.INVISIBLE);
                    eurSlika.setVisibility(View.INVISIBLE);
                    gbpSlika.setVisibility(View.VISIBLE);
                    chfSlika.setVisibility(View.INVISIBLE);
                    rsdSlika.setVisibility(View.INVISIBLE);
                    textView11.setText("Izabrana valuta je GBP");
                    String BAM,proviz="0.00";
                    double knovca,kurs=2.12670;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    int provjera;
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else {
                        double kursnarazlika = 0.0773 * knovca;
                        double bam = (knovca * kurs);
                        bam = Math.round(bam * 100.0) / 100.0;
                        kursnarazlika = Math.round(kursnarazlika * 100.0) / 100.0;
                        BAM = String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM) + " BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }
            }
        });
        vusd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //usd
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.INVISIBLE);
                    cadSlika.setVisibility(View.INVISIBLE);
                    hrkSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.INVISIBLE);
                    nokSlika.setVisibility(View.INVISIBLE);
                    sekSlika.setVisibility(View.INVISIBLE);
                    trySlika.setVisibility(View.INVISIBLE);
                    jpySlika.setVisibility(View.INVISIBLE);
                    usdSlika.setVisibility(View.VISIBLE);
                    eurSlika.setVisibility(View.INVISIBLE);
                    gbpSlika.setVisibility(View.INVISIBLE);
                    chfSlika.setVisibility(View.INVISIBLE);
                    rsdSlika.setVisibility(View.INVISIBLE);
                    textView11.setText("Izabrana valuta je USD");
                    String BAM,proviz="0.00";
                    double knovca,kurs=1.55368;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    int provjera;
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else {
                        double kursnarazlika = 0.03992 * knovca;
                        double bam = (knovca * kurs);
                        bam = Math.round(bam * 100.0) / 100.0;
                        kursnarazlika = Math.round(kursnarazlika * 100.0) / 100.0;
                        BAM = String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM) + " BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }
            }
        });
        vrsd.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //rsd
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.INVISIBLE);
                    cadSlika.setVisibility(View.INVISIBLE);
                    hrkSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.INVISIBLE);
                    nokSlika.setVisibility(View.INVISIBLE);
                    sekSlika.setVisibility(View.INVISIBLE);
                    trySlika.setVisibility(View.INVISIBLE);
                    jpySlika.setVisibility(View.INVISIBLE);
                    usdSlika.setVisibility(View.INVISIBLE);
                    eurSlika.setVisibility(View.INVISIBLE);
                    gbpSlika.setVisibility(View.INVISIBLE);
                    chfSlika.setVisibility(View.INVISIBLE);
                    rsdSlika.setVisibility(View.VISIBLE);
                    textView11.setText("Izabrana valuta je RSD");
                    String BAM,proviz="0.00";
                    double knovca,kurs=1.58179;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    knovca=knovca/100;
                    int provjera;
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else {
                        double kursnarazlika = 0.06605 * knovca;
                        double bam = knovca * kurs;
                        bam = Math.round(bam * 100.0) / 100.0;
                        kursnarazlika = Math.round(kursnarazlika * 100.0) / 100.0;
                        BAM = String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM) + " BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }
            }
        });
        vtry.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //try
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.INVISIBLE);
                    cadSlika.setVisibility(View.INVISIBLE);
                    hrkSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.INVISIBLE);
                    nokSlika.setVisibility(View.INVISIBLE);
                    sekSlika.setVisibility(View.INVISIBLE);
                    trySlika.setVisibility(View.VISIBLE);
                    jpySlika.setVisibility(View.INVISIBLE);
                    usdSlika.setVisibility(View.INVISIBLE);
                    eurSlika.setVisibility(View.INVISIBLE);
                    gbpSlika.setVisibility(View.INVISIBLE);
                    chfSlika.setVisibility(View.INVISIBLE);
                    rsdSlika.setVisibility(View.INVISIBLE);
                    textView11.setText("Izabrana valuta je TRY");
                    String BAM,proviz="0.00";
                    double knovca,kurs=0.38642;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    int provjera;
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else {
                        double kursnarazlika = 0.02998 * knovca;
                        double bam = (knovca * kurs);
                        bam = Math.round(bam * 100.0) / 100.0;
                        kursnarazlika = Math.round(kursnarazlika * 100.0) / 100.0;
                        BAM = String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM) + " BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }
            }
        });
        veur.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                //eur
                try{
                    zemSlika.setVisibility(View.INVISIBLE);
                    audSlika.setVisibility(View.INVISIBLE);
                    cadSlika.setVisibility(View.INVISIBLE);
                    hrkSlika.setVisibility(View.INVISIBLE);
                    dkkSlika.setVisibility(View.INVISIBLE);
                    nokSlika.setVisibility(View.INVISIBLE);
                    sekSlika.setVisibility(View.INVISIBLE);
                    trySlika.setVisibility(View.INVISIBLE);
                    jpySlika.setVisibility(View.INVISIBLE);
                    usdSlika.setVisibility(View.INVISIBLE);
                    eurSlika.setVisibility(View.VISIBLE);
                    gbpSlika.setVisibility(View.INVISIBLE);
                    chfSlika.setVisibility(View.INVISIBLE);
                    rsdSlika.setVisibility(View.INVISIBLE);
                    textView11.setText("Izabrana valuta je EUR");
                    String BAM,kursnarazlika="0.00";
                    double proviz,knovca,kurs=1.95583;
                    knovca=Integer.valueOf(iznos.getText().toString());
                    int provjera;
                    provjera=Integer.valueOf(iznos.getText().toString());
                    if(provjera==0){
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(MainActivity.this);
                        builder1.setTitle("Upozorenje");
                        builder1.setMessage("Valutni iznos ne moze biti 0!");
                        builder1.setCancelable(true);
                        builder1.setNeutralButton(android.R.string.ok,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        return;
                    }
                    else {
                        proviz = 0.01077;
                        double bam = (kurs - proviz) * knovca;
                        bam = Math.round(bam * 100.0) / 100.0;
                        BAM = String.valueOf(bam);
                        isplata.setText(String.valueOf(BAM) + " BAM");
                        krazlika.setText(String.valueOf(kursnarazlika));
                        provizija.setText(String.valueOf(proviz));
                    }
                }catch(Exception e){
                    AlertDialog.Builder bilder = new AlertDialog.Builder(MainActivity.this);
                    bilder.setTitle("Upozorenje");
                    bilder.setMessage("Prvo unesite valutni iznos!");
                    bilder.setCancelable(true);
                    bilder.setNeutralButton(android.R.string.ok,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                    AlertDialog alert11=bilder.create();
                    alert11.show();
                }
            }
        });
    }
    @Override
    protected void onPause(){
        Log.i("INFO","Pause");
        super.onPause();
    }
    @Override
    public void onBackPressed(){
        String poruka="Da li ste sigurni da želite napustiti \"Mjenjačnicu\"?";
        AlertDialog.Builder pazise=new AlertDialog.Builder(this);
        pazise.setTitle("Izlaz")
                .setMessage(poruka)
                .setNegativeButton("Ne",null)
                .setPositiveButton("Da", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        MainActivity.super.onBackPressed();
                    }
                }).create().show();
    }
}

